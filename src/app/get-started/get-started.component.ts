import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { GondolaService } from '../../app/gondola.service';  
import { zip } from 'rxjs/operators';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.scss']
})
export class GetStartedComponent implements OnInit {
  /*
  * To View the Duration Amount Page view
  */
  isDurationAmountView = true;
  /*
  * To View the Duration years Page view
  */
  isDurationYearsView = false;
  /*
  * To View the Personal details Page view
  */
  isPersonalDetailsView = false;
  /*
  * To submit the form to stored in api
  */
  isSubmitDone = false;
  /*
  * To store the the maximum date value in datepicker
  */
  maxDate = new Date();
  /*
  * To store the minimum date value in datepicker
  */
  minDate = new Date(new Date().getFullYear()-60);
  
  /*
  * To store the value of
  */  
	durationamount:number = 100000;
  /*
  * To store the value of
  */  
	durationyears: number = 0;
  /*
  * To store the value of
  */  
	annualincome = '';
  /*
  * To store the value of
  */  
	expectedretirementage = '';
  /*
  * To store the value of
  */  
	firstname = '';
  /*
  * To store the value of
  */  
	middeintial = '';
  /*
  * To store the value of
  */  
	lastname = '';
  /*
  * To store the value of
  */  
	smokeortobaccouse = 'No';
  /*
  * To store the value of
  */  
	dob = '';
  /*
  * To store the value of
  */  
	zipcode = '';
  /*
  * To store the value of
  */  
	gender = '';
   /*
  * To store the value of
  */  
	progressBar = '50';
  /**
   *  duration from group
   */
   durationForm : FormGroup;
  /**
   *  persoanl from group
   */
    personalDetailsForm : FormGroup;
  /**
   *  variable
  */
    isGenderEmpty= false;

  constructor(private router: Router,
    private gondolaService:GondolaService,
    private formBuilder: FormBuilder) { }


  ngOnInit(): void {

    this.durationForm = this.formBuilder.group({
      durationAmount: new FormControl({ value: '' },),
      durationRange: new FormControl('',{ 
      updateOn: "submit" }),
      annualIncome:  new FormControl('', { validators: [this.validateAnnualIncome()], 
        updateOn: 'submit' },),
      retirementAge:  new FormControl('', { validators: [this.validateRetirementAge()], 
        updateOn: 'submit' },),
      });
      this.personalDetailsForm = this.formBuilder.group({
        middeintial: new FormControl({ value: '' },),
        lastname: new FormControl({ value: '' },),
        firstname: new FormControl('', { validators: [this.validateFirstName()], 
          updateOn: "submit" }),
        dob:  new FormControl('', { validators: [this.validateDob()], 
          updateOn: 'submit' },),
        zipcode:  new FormControl('', { validators: [this.validateZipCode()], 
          updateOn: 'submit' },),
        });
  }

  setIsSmoker(value: string){
    this.smokeortobaccouse = value;
  }
  setGender(value: string){
    this.gender = value;
  }

  updateSetting(event: any) {
    this.durationamount = event.value;
  }

  updateYearsSetting(event: any) {
    this.durationyears = event.value;
  }

  numberWithCommas(x: number) {
    let parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts;
  }

  navigateDurationYears() {
    this.isDurationAmountView = false;
    this.isDurationYearsView = true;
  }
  navigateToPersonalDetails(){
    this.progressBar = '80';
    this.isDurationAmountView = false;
    this.isDurationYearsView = false;
    this.isPersonalDetailsView = true;
  }
  backToDurationAmount(){
    this.isDurationAmountView = true;
    this.isDurationYearsView = false;
    this.isPersonalDetailsView = false;
  }

  submitQuotes() {
    const QuoteObject = this.createNewQuoteObject();
    this.isDurationAmountView = false;
    this.isDurationYearsView = false;
    this.isPersonalDetailsView = false;
    this.isSubmitDone = true;
    console.log("QuoteObject",QuoteObject)
    // this.gondolaService.createQuotes(QuoteObject)  
    //   .subscribe(data => console.log(data), error => console.log(error));
  }

  backToDurationYears() {
    this.isDurationAmountView = false;
    this.isDurationYearsView = true;
    this.isPersonalDetailsView = false;
  }
  submitDurationForm(){
    this.durationForm.get('durationRange')?.setValidators(this.validateDurationRange());
    this.durationForm.get('durationRange')?.updateValueAndValidity();
    if(this.durationForm.valid){
      this.navigateToPersonalDetails();
    }
  }
  submitPersonalForm(){
      if(!this.gender){
        this.isGenderEmpty = true;
      }else{
        this.isGenderEmpty = false;
      }
    if(this.personalDetailsForm.valid){
        this.submitQuotes();
    }
  }
  checkInputValue(event: any):boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  allowOnlyAlphabet(event: any):boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 15 && charCode <= 64) || (charCode >= 123) || (charCode <= 96 && charCode >= 91)) {
      return false;
    }
    return true;
  }
  createNewQuoteObject():any{
    const requestObject: any = {};
    requestObject.durationamount = this.durationamount;
    requestObject.durationyears = this.durationyears;
    requestObject.annualincome = this.annualincome;
    requestObject.expectedretirementage = this.expectedretirementage;
    requestObject.firstname = this.firstname;
    requestObject.middeintial = this.middeintial;
    requestObject.lastname = this.lastname;
    requestObject.smokeortobaccouse = this.smokeortobaccouse;
    requestObject.dob = this.calculateAgeFromDOB(this.dob);
    requestObject.state = this.fetchState(this.zipcode);
    requestObject.gender = this.gender;
    return requestObject;
  }
  public calculateAgeFromDOB(dob: any): number {
    const today = new Date();
    const birthDate = new Date(dob);
    let age = today.getFullYear() - birthDate.getFullYear();
    const month = today.getMonth() - birthDate.getMonth();

    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    return age;
  }

  validateAnnualIncome(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null=> {
        const income = control.value;
        if (!income && parseFloat(income) !==0) {
          
            return { 'required': true };
        }else if(income <="0"){
          return { 'minIncome': true };
        }
        return null;
    };
}
validateRetirementAge(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      const age = control.value;
      if (!age && parseFloat(age) !==0) {
          return { 'required': true };
      }else if(age <="0"){
        return { 'minAge': true };
      }
      return null;
  };
}
validateDurationRange(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      const range = control.value;
      if (range === null || 
        range === undefined || 
        range === 0 || 
        range === NaN) {
          return { 'required': true };
      }
      return null;
  };
}
validateFirstName(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      const fname = control.value;
      if (!fname) {
          return { 'required': true };
      }
      return null;
  };
}
validateDob(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      const dob = control.value;
      if (!dob) {
          return { 'required': true };
      }
      return null;
  };
}
validateZipCode(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
      const zipCode = control.value;
      const state = this.fetchState(zipCode);
      if (zipCode === null || zipCode === undefined || zipCode === '' || zipCode === NaN) {
          return { 'required': true };
      }else if(state !== null && state !== undefined && state !== ''&& state === "none"){
        return { 'invalid': true };
      }
      return null;
  };
}
	fetchState(zipString:string): string {
    /* Ensure we don't parse strings starting with 0 as octal values */
    const zipcode = parseInt(zipString, 10);
    let st;
    let state;
    /* Code cases alphabetized by state */
    if (zipcode >= 35004 && zipcode <= 36925) {
        st = 'AL';
        state = 'Alabama';
    } else if (zipcode >= 99501 && zipcode <= 99950) {
        st = 'AK';
        state = 'Alaska';
    } else if (zipcode >= 85001 && zipcode <= 86556) {
        st = 'AZ';
        state = 'Arizona';
    } else if (zipcode >= 71601 && zipcode <= 72959) {
        st = 'AR';
      state = 'Arkansas';
    } else if (zipcode >= 90001 && zipcode <= 96162) {
        st = 'CA';
        state = 'California';
    } else if (zipcode >= 80001 && zipcode <= 81658) {
        st = 'CO';
        state = 'Colorado';
    } else if ((zipcode >= 6001 && zipcode <= 6389) || (zipcode >= 6401 && zipcode <= 6928)) {
        st = 'CT';
        state = 'Connecticut';
    } else if (zipcode >= 19701 && zipcode <= 19980) {
        st = 'DE';
        state = 'Delaware';
    } else if (zipcode >= 32004 && zipcode <= 34997) {
        st = 'FL';
        state = 'Florida';
      } else if ( (zipcode >= 30001 && zipcode <= 31999) || (zipcode >= 39901 && zipcode <= 39901) ) {
        st = 'GA';
        state = 'Georgia';
    } else if (zipcode >= 96701 && zipcode <= 96898) {
        st = 'HI';
        state = 'Hawaii';
    } else if (zipcode >= 83201 && zipcode <= 83876) {
        st = 'ID';
        state = 'Idaho';
    } else if (zipcode >= 60001 && zipcode <= 62999) {
        st = 'IL';
        state = 'Illinois';
    } else if (zipcode >= 46001 && zipcode <= 47997) {
        st = 'IN';
        state = 'Indiana';
    } else if (zipcode >= 50001 && zipcode <= 52809 ) {
        st = 'IA';
        state = 'Iowa';
    } else if (zipcode >= 66002 && zipcode <= 67954) {
        st = 'KS';
        state = 'Kansas';
    } else if (zipcode >= 40003 && zipcode <= 42788) {
        st = 'KY';
        state = 'Kentucky';
    } else if (zipcode >= 70001 && zipcode <= 71497) {
        st = 'LA';
        state = 'Louisiana';
    } else if (zipcode >= 3901 && zipcode <= 4992) {
        st = 'ME';
        state = 'Maine';
    } else if ((zipcode >= 20601 && zipcode <= 21930) || (zipcode == 20588)){
        st = 'MD';
        state = 'Maryland';
    } else if ( (zipcode >= 1000 && zipcode <= 2791) || (zipcode == 5501) || (zipcode == 5544 )) {
        st = 'MA';
        state = 'Massachusetts';
    } else if (zipcode >= 48001 && zipcode <= 49971) {
        st = 'MI';
        state = 'Michigan';
    } else if (zipcode >= 55001 && zipcode <= 56763) {
        st = 'MN';
        state = 'Minnesota';
    } else if (zipcode >= 38601 && zipcode <= 39776) {
        st = 'MS';
        state = 'Mississippi';
    } else if (zipcode >= 63001 && zipcode <= 65899) {
        st = 'MO';
        state = 'Missouri';
    } else if (zipcode >= 59001 && zipcode <= 59937) {
        st = 'MT';
        state = 'Montana';
    } else if (zipcode >= 27006 && zipcode <= 28909) {
        st = 'NC';
        state = 'North Carolina';
    } else if (zipcode >= 58001 && zipcode <= 58856) {
       st = 'ND';
        state = 'North Dakota';
    } else if ((zipcode >= 68001 && zipcode <= 68020) || (zipcode >= 68022 && zipcode <= 69367)){
        st = 'NE';
        state = 'Nebraska';
    } else if (zipcode >= 88901 && zipcode <= 89883) {
        st = 'NV';
        state = 'Nevada';
    } else if (zipcode >= 3031 && zipcode <= 3897) {
        st = 'NH';
        state = 'New Hampshire';
    } else if (zipcode >= 7001 && zipcode <= 8989) {
        st = 'NJ';
        state = 'New Jersey';
    } else if (zipcode >= 87001 && zipcode <= 88441) {
        st = 'NM';
        state = 'New Mexico';
    } else if (zipcode >= 2801 && zipcode <= 2940) {
        st = 'RI';
        state = 'Rhode Island';
    } else if (zipcode >= 29001 && zipcode <= 29948) {
        st = 'SC';
        state = 'South Carolina';
    } else if (zipcode >= 57001 && zipcode <= 57799) {
        st = 'SD';
        state = 'South Dakota';
    } else if (zipcode >= 37010 && zipcode <= 38589) {
        st = 'TN';
        state = 'Tennessee';
    } else if ((zipcode == 73301) || ( zipcode ==75501) || (zipcode >= 75003 && zipcode <= 79999) || (zipcode >= 88510 && zipcode <= 88589) ) {
        st = 'TX';
        state = 'Texas';
    } else if (zipcode >= 84001 && zipcode <= 84784) {
        st = 'UT';
        state = 'Utah';
    } else if ((zipcode >= 5001 && zipcode <= 5495) || (zipcode >= 5601 && zipcode <= 5907)){
        st = 'VT';
        state = 'Vermont';
    } else if ( (zipcode >= 20040 && zipcode <= 20167) || (zipcode >= 22001 && zipcode <= 24658) || (zipcode == 20598) ) {
        st = 'VA';
        state = 'Virginia';
    } else if ( (zipcode >= 20001 && zipcode <= 20091) || (zipcode >= 20201 && zipcode <= 20599) || (zipcode >= 56901 && zipcode <= 56904) ) {
        st = 'DC';
        state = 'Washington DC';
    } else if (zipcode >= 98001 && zipcode <= 99403) {
        st = 'WA';
        state = 'Washington';
    } else if (zipcode >= 24701 && zipcode <= 26886) {
        st = 'WV';
        state = 'West Virginia';
    } else if (zipcode >= 53001 && zipcode <= 54990) {
        st = 'WI';
        state = 'Wisconsin';
    } else if (zipcode >= 82001 && zipcode <= 83128) {
        st = 'WY';
        state = 'Wyoming';
    } else {
        st = 'none';
        state = 'none';
    }
    return state;
  }
}