import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs';  

@Injectable({
  providedIn: 'root'
})
export class GondolaService {

  private baseUrl = 'http://localhost:8080/gondola/api/v1/';  
  
  constructor(private http:HttpClient) { }  
  
  
  createQuotes(quotes: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'quotes', quotes);  
  }  
}
